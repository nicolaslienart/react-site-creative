# Refaire un site semblable à https://monplus.ca/

## Technologies utilisées :

### React.js

Avec son router

### Sass
### Three.js

Pour le logo plus en fond sur la deuxième section de la page d'accueil

### anime.js

Pour l'animation de chargement, le reste des animations sont en CSS

## Instructions

`nvm use`
`npm install`
`npm start`

`http://localhost:3000/`

ou

`http://localhost:3000/video`
