import './MenuEntry.scss'
import svg from '../imgs/sprite.svg'
function MenuEntry(props) {
	return (
		<li className="menu-entry" style={{ backgroundImage: `url(${props.image})` }}>
			<span>{props.title}</span>
			<i>
				<svg className="svg-arrow-right">
					<use href={`${svg}#svg-arrow-right`}></use>
				</svg>
			</i>
		</li>
	)
}

export default MenuEntry
