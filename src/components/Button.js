import React from 'react'
import './Button.scss'

function Button({title, border, color, style}) {
	const buttonStyle = {
		...style,
		border: border ? `1px solid ${border}` : 'none',
		fontWeight: color?'bold':'initial',
		color: color ? 'white':'inherit'
	} 
	return (
		<span className="button" style={buttonStyle}> 
			<p className="behind" style={{ color: color ? color : 'inherit' }}>{title}</p>
			<div style={{ background: color ? color : 'white' }} className="button-shutter" />
			<p>{title}</p>
		</span>
	)
}


export default Button
