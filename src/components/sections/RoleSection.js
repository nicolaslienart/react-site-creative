import React from 'react'

import './RoleSection.scss'

const role = require('./role.json')

export default class RoleSection extends React.Component {
	render() {
		return (
			<div className="role-section">
				<div className="role-grid">
					<span id="role-elargi">Rôle élargi</span>
					<span id="apprendre-title">Apprendre pour aller plus loin</span>
					{
						role.map(role => (
							<div className="role-box">
								<img src={role.image} alt=""/>
								<span className="title-role">{ role.name }</span>
								<p className="desc-role">{ role.description }</p>
							</div>
						))
					}
				</div>
			</div>
		)
	}
}
