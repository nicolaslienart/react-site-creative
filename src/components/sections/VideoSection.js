import React from 'react'

import './VideoSection.scss'

class VideoSection extends React.Component {
	render() {
		return (
			<div class="video-section">
				<div className='vimeo-container'><iframe src="https://player.vimeo.com/video/375295275" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" title="quebec video" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
			</div>
		)
	}
}

export default VideoSection
