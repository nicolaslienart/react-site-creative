import React from "react";

import Button from '../Button'
import './FirstSection.scss'
//import SVGPlus from '../../imgs/plus.svg'

class FirstSection extends React.Component {

	constructor() {
		super()
		this.state = {
			scroll: 0
		}
	}

	computeLandscapeZoom() {

		return {
			backgroundSize: `${(this.state.scroll / document.documentElement.clientHeight) * 20 + 100}%`
		}
	}

	componentDidMount() {
		window.addEventListener('scroll', listener => {
			this.setState({ scroll: document.scrollingElement.scrollTop })
		})
	}

	computeWomenZoom() {
		return {
			backgroundSize: `${120 - (this.state.scroll / document.documentElement.clientHeight) * 20}%`
		}
	}

	computePlusRotation() {
		const clientHeight = document.documentElement.clientHeight
		return {
			transform: `rotateZ(${this.state.scroll / clientHeight * 90}deg)`
		}
	}

	computeWidth() {
		return {
			transform: `scaleX(${0.5 + this.state.scroll / 600 /2})`
		}
	}

	computePositionShift() {
		const shift = this.state.scroll / 100
		return {
			backgroundPosition: `${shift}% -${shift * 1.5}%`
		}
	}

	render() {
		return (
			<div className="sec-1">
				<div id="content-zone">
					<h1>Ton partenaire de choix pour t’aider à <br/><strong>rayonner</strong></h1>
					<p>Fondée en 2001, MON+ est une agence de placement experte dans le domaine de la santé dédiée à offrir des services de qualité et une approche humaine exceptionnelle.
</p>
					<div>
						<div></div>
						<span>Partenaire d'ici</span>
					</div>
					<div>
						<Button style={{ borderTopRightRadius: '0', borderBottomRightRadius: '0' }} color='#5fb727' title='Trouver un emploi'/>
						<Button style={{ borderTopLeftRadius: '0', borderBottomLeftRadius: '0' }} color='#5a9b84' title='Trouver une ressource'/>
					</div>
				</div>
				<div className="decorative-zone">
					<div className="decorative">
						<span style={this.computeLandscapeZoom()} className="landscape-circle"  ></span>
						<span className="dots" style={this.computePositionShift()}></span>
						<span style={this.computeWomenZoom()} className="women-square"></span>
						<svg style={this.computePlusRotation()} viewBox="0 0 60 60" className="svg-plus">
							<path fill="#5fb727" d="M30 60c16.6 0 30-13.4 30-30S46.6 0 30 0 0 13.4 0 30s13.4 30 30 30z"></path><path fill="#fff" d="M33.3 27h17.1v5.3H33.3v17.2H28V32.3H10.9V27H28V9.9h5.3V27z"></path>
						</svg>
						<div className="hor-bars">
							<span><span style={this.computeWidth()} className="inner"></span></span>
							<span><span style={this.computeWidth()} className="inner"></span></span>
							<span><span style={this.computeWidth()} className="inner"></span></span>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default FirstSection
