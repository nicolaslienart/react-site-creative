import './Footer.scss'

function Footer() {
	return (
		<footer>
			<span id="copyright">© Multi Options Nursing 2021, Tous droits réservés</span>
			<span id="note">Note: Dans nos textes, le féminin inclut le masculin et est utilisé, sans discrimination, afin d’alléger le texte et faire référence à nos ressources, majoritairement constituées de femmes.</span>
		</footer>
	)
}

export default Footer
