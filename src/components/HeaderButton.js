import React from 'react'

import './HeaderButton.scss'

class HeaderButton extends React.Component {
	render() {
		return (
			<span>
				<span className="nav-button">
					{this.props.title}
				</span>
				{this.props.children}
			</span>
		)
	}
}

export default HeaderButton
