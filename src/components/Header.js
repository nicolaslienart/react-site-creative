import React from 'react'
import { BrowserRouter, Link } from 'react-router-dom'

import './Header.scss'
import HeaderButton from './HeaderButton.js'
import Button from './Button.js'
import Menu from './Menu.js'


class Header extends React.Component {

	render() {
		const animals = ["Dog", "Bird", "Cat", "Mouse", "Horse"];
		return (
			<header className={this.props.lock ? 'lock' : ''}>
				<div id="header-bg" className={this.props.lock ? 'show' : ''}></div>
				<div className="header-content">
					<div id="header-logo">
						<div id="img-logo">
						</div>
					</div>
					<nav>
						<ul>
							<BrowserRouter>
								{animals.map(( animal, index ) => {
									if (animal !== 'Dog') {
										return (
											<li key={index}>
												<Link to='/blank'>
													<HeaderButton title={ animal } />
												</Link>
											</li>
										)
									} else
										return (
											<li id="dog" key={index}>
												<Link to='/'>
													<HeaderButton title={ animal }>
													<Menu />
													</HeaderButton>
												</Link>
											</li>
										)
								})}
								<li>
												<Link to='/video'>
													<Button title='EN' border='#e4efe9' />
												</Link>
								</li>
							</BrowserRouter>
							</ul>
						</nav>
			</div>
			</header>
		)
	}
}

export default Header
