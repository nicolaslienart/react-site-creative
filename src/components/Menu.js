import React from 'react'

import MenuEntry from './MenuEntry.js'
import './Menu.scss'

const entries = require('./entries.json')


class Menu extends React.Component {
	render() {
		return (
			<ul id="menu">
				{
					entries.map(entry => (
						<MenuEntry key={entry.title} title={entry.title} image={entry.img}/>
					))
				}
			</ul>
		)
	}
}

export default Menu
