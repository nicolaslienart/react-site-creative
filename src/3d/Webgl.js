import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { Scene, PerspectiveCamera, WebGLRenderer, AmbientLight, DirectionalLight, Vector2, MeshLambertMaterial, FrontSide } from 'three'

import plus from './objects/plus.gltf'

export default class Webgl {
	constructor(section) {
		this.start = this.start.bind(this)
		this.onResize = this.onResize.bind(this)

		this.scene = new Scene()
		this.camera = new PerspectiveCamera( 75, 1.5, 0.1, 1000 )
		// this.camera = new OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, 1, 10000 );
		this.renderer = new WebGLRenderer({
			alpha: true,
			powerPreference: "high-performance",
			antialias: false,
			stencil: false,
			depth: false
		})
		this.renderer.setSize(section.clientWidth, section.clientWidth / 2)
		section.appendChild(this.renderer.domElement)
		//this.canvas = canvas
		//this.renderer.setSize( window.innerWidth, window.innerHeight );

		//this.renderer.setClearColor( 0xffff55, 0);

		this.light = new AmbientLight( 0x404040, 1 ); // soft white light
		this.scene.add(this.light);

		const color = 0xFFFFFF;
		const intensity = 0.5;
		const light = new DirectionalLight(color, intensity);
		light.position.set(0, 10, 3);
		light.target.position.set(-5, 0, 0);
		this.scene.add(light);
		this.scene.add(light.target);

		this.mouse = new Vector2()

		window.addEventListener('mousemove', pointer => {
			this.mouse.x = pointer.screenX
			this.mouse.y = pointer.screenY
		})

		const loader = new GLTFLoader();
		loader.load(plus, (gltf) => {
			gltf.scene.traverse( ( object ) => {
				var meshBasicMaterial = new MeshLambertMaterial({
					color: 0xe7f2ec,
					side: FrontSide,
					emissive:0x334433 
				});
				if ( object.isMesh ) {
					this.plus = object
					object.material = meshBasicMaterial
					object.scale.set( 4, 4, 0.4 );
					object.position.set(0,0,0)
					//object.rotation.x = 45
				}

			})
			this.scene.add(gltf.scene)
			

		}, undefined, function ( error ) {
			console.error( error );
		} );

		this.camera.position.z = 12;
		this.camera.position.y = 2;
		this.camera.position.x = -2;

		this.time = 0

		window.addEventListener('resize', this.onResize);

		this.effects = {}
	}

	onResize () {
		this.camera.aspect = window.innerWidth / window.innerHeight
		this.camera.updateProjectionMatrix()
		this.renderer.setSize( window.innerWidth, window.innerHeight )
	}

	start () {
		requestAnimationFrame( this.start );
		if (this.canvasSize === undefined)
			this.canvasSize = new Vector2(this.renderer.domElement.width, this.renderer.domElement.height)
		this.time += 0.01;
		if (this.plus) {
			//console.log(this.canvas.width)
			const rotationX = ((2/this.canvasSize.x) * this.mouse.y - 1) * (Math.PI / 4) 
			//this.plus.rotation.x = (this.mouse.y - this.canvas.height / 2)/this.canvas.height / 5
			const rotationY = ((2/this.canvasSize.y) * this.mouse.x - 1) * (Math.PI / 4) /2 -45
			if (this.plus.rotation.y !== rotationY) {
				//console.log(this.plus.rotation)
				this.plus.rotation.y = rotationY
			}
			if (this.plus.rotation.x !== rotationX)
				this.plus.rotation.x = rotationX
			//this.plus.rotation.y = -(this.mouse.x - this.canvas.width / 2)/this.canvas.width / 5
		}
		this.renderer.render( this.scene, this.camera );
	}
}

