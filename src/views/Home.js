// Components
import FirstSection from "../components/sections/FirstSection";
import SecondSection from "../components/sections/SecondSection";

export default function Home() {
	return (
		<div>
			<FirstSection />
			<SecondSection />
		</div>
	)
}
