import VideoSection from '../components/sections/VideoSection'
import RoleSection from '../components/sections/RoleSection'

export default function Video() {
	return (
		<div>
			<VideoSection />
			<RoleSection />
		</div>
	)
}
