import React from "react";
import { Switch, Route } from 'react-router-dom'


import "./App.css";

import anime from 'animejs/lib/anime.es.js';

// Views
import Home from './views/Home'
import Blank from './views/Blank'
import Video from './views/Video'

// Components
import Header from "./components/Header";
import Footer from './components/Footer'

import { CSSTransition } from 'react-transition-group';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lockHeader: false,
		 loaded: false
    };
	  this.plus = React.createRef()
	  this.loader = React.createRef()
	  window.onload = () => { this.setState({ loaded: true }) }
  }
	componentDidMount() {
		window.addEventListener("scroll", () => {
			if (document.scrollingElement.scrollTop > 0)
				this.setState({ lockHeader: true });
			else 
				this.setState({ lockHeader: false });
		});

		anime({
			targets: this.plus.current,
			loop: true,
			easing: 'easeInOutQuad',
			rotate: 90,
			duration: 1500,
			timeout: 500,
		})
		anime({
			targets: '.loader .plus',
			easing: 'easeInCubic',
			width: '50px',
			duration: 800,
			timeout: 700,
		})
		anime({
			targets: '#half-band',
			easing: 'easeInCubic',
			width: '50%',
			duration: 800,
		})
	}

  render() {
    return (
		 <CSSTransition onEntered={() => this.loader.current.style.display = 'none'} timeout={1000} in={this.state.loaded} classNames="example">
				 <div>
					 <div ref={this.loader} className="loader">
						 <div id="half-band"></div>
						 <div className="plus">
							 <svg ref={this.plus} viewBox="0 0 60 60" className="svg-plus">
							 <path fill="#5fb727" d="M30 60c16.6 0 30-13.4 30-30S46.6 0 30 0 0 13.4 0 30s13.4 30 30 30z"></path><path fill="#fff" d="M33.3 27h17.1v5.3H33.3v17.2H28V32.3H10.9V27H28V9.9h5.3V27z"></path>
							 </svg>
						 </div>
					 </div>
					 <div className="App">
						 <Header lock={this.state.lockHeader} />
						 <main>
							 <Switch>
								 <Route path='/blank' component={Blank} />
								 <Route path='/video' component={Video} />
								 <Route exact path='/' component={Home} />
								 <Route component={Blank} />
							 </Switch>
						 </main>
						 <Footer />
					 </div>
				 </div>
		 </CSSTransition>
    );
  }
}


export default App;
